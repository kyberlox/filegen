from docx import Document
from docx.shared import Pt, Mm

from docx2pdf import convert

doc = Document()

# задаем стиль текста по умолчанию
styleTitle = doc.styles['Normal']
# название шрифта
styleTitle.font.name = 'Times New Roman'
# размер шрифта
styleTitle.font.size = Pt(14)



#doc.add_picture('logo.png', Mm(25), Mm(20))
doc.add_paragraph('Опросный лист', 'Title')



prt = doc.add_paragraph('\n Заказчик ', styleTitle)
pt = ['Организация', 'Объект установки', 'Ф.И.О.', 'Телефон контактного лица', 'email']
pi = ['наименование компании', 'объект',  'Ф.И.О.', 'телефон', 'email']
table = doc.add_table(rows=0, cols=2)
table.style = 'Table Grid'
for i in range(len(pt)):
    row_cells = table.add_row().cells
    row_cells[0].text = pt[i]
    cell = table.cell(i, 1)
    cell.text = pi[i]



prt1 = doc.add_paragraph('\n Характеристики арматуры', styleTitle)
pt = ['Маркировка', 'Тип арматуры', 'Режим работы', 'Требуемое время закрытия арматуры', 'Температура окружающей среды', 'Крутящий момент с зспасом, Нм ']
pi = ['маркировка',  'тип арматуры', ' запорный \ запорно-регулирующий \ регулирующий', ' сек', 'мин. \ °C макс. \ °C ', ' Нм ']

table1 = doc.add_table(rows=0, cols=2)
table1.style = 'Table Grid'
for i in range(len(pt)):
    row_cells = table1.add_row().cells
    row_cells[0].text = pt[i]
    cell = table1.cell(i, 1)
    cell.text = pi[i]
    


prt2 = doc.add_paragraph('\n Параметры присоединения арматуры под привод ', styleTitle)

pt1 = ['Тип', 'Количество оборотов на закрытие', 'Размер фланца  ОСТ ЦКБП 062-2009 (ОСТ 26-07-763-73)', 'Выходной вал ОСТ ЦКБП 062-2009 (ОСТ 26-07-763-73)', 'Размер фланца ISO5210', 'Тип втулки ISO5210', 'Размер штока для типа Д по ОСТ']
pi1 = ['МНОГООБОРОТНАЯ АРМАТУРА', 'обороты', '', 'квадрат / кулачёк', '', '', '']

pt2 = ['Тип', 'Размер фланца (ISO 5211), F', ' Угол поворота ', 'Тип обработки втулки', 'Чертёж прилагается']
pi2 = ['ЧЕТВЕРТЬОБОРОТНАЯ АРМАТУРА', ' F ', '', '', '', '']

pt3 = ['Тип', 'Ход штока / размеры']
pi3 = ['ПРЯМОХОДНАЯ АРМАТУРА', ' ']

at = 'Многооборотная арматура'
arm_types = ['Многооборотная арматура', 'Четвертьоборотная арматура', 'Прямоходная арматура']
if at == arm_types[0]:
    pt = pt1
    pi = pi1
elif at == arm_types[1]:
    pt = pt2
    pi = pi2
elif at == arm_types[2]:
    pt = pt3
    pi = pi3

table2 = doc.add_table(rows=0, cols=2)
table2.style = 'Table Grid'

for i in range(len(pt)):
    row_cells = table2.add_row().cells
    row_cells[0].text = pt[i]
    cell = table2.cell(i, 1)
    cell.text = pi[i]



doc.add_page_break()



prt3 = doc.add_paragraph('\n Характеристики электропривода ', styleTitle)

pt = ['Напряжение питания', 'Исполнение привода', 'Защита IP', 'Блок управления', 'Управление', 'Концевые выключатели', 'Промежуточные выключатели (опция)', 'Моментные выключатели', 'Механический указатель положения', 'Дистанционный указатель положения', 'Цвет покрытия']

pi = ['380 В / 50Гц / 3ф \n 220 В / 50Гц / 1ф только для приводов ЭПН \n 24 В DC1ф только для приводов ЭПН \n  660 только для приводов ЭП4 до1500Нм', 
      'общепромышленное \n взрывозащищенное (1ExdeIIВT4 - стандарт) \n авзрывозащищенное (1ExdeIIСT4 )  только для приводов ЭП4до 2000Нм \n шахтное (PB ExdI) только для приводов ЭП4 \n морское (М5)', 
      'IP67 / IP68', 
      u'с электромеханическим блоком концевых выключателей (МБКВ) М1 \n с электронным блоком концевых выключателей без встроенного пускателя (ЭБКВ) Э2 \n с электронным интелектуальным модулем управления встроенный пускатель (ЭИМУ) Э1 \n выносной блок управления встроенный пускатель ( ВИМУ кабель 10м стандарт) Э0', 
      '24 В DC / 4…20 мА / Modbus RTU / Profibus DP / 220В АС', 
      'одиночные (стандарт) / сдвоенные только блок Э1', 
      'одиночные / сдвоенные только блок Э1', 
      'одиночные (стандарт) / сдвоенные только блок Э1', 
      'если да, то вылет штока (обязательно) / нет (стандарт)', 
      '(4-20 мА) / Потенциометр / 24В', 
      'серый / указать цвет корпуса и цвет ручного дублера']

table3 = doc.add_table(rows=0, cols=2)
table3.style = 'Table Grid'
for i in range(len(pt)):
    row_cells = table3.add_row().cells
    row_cells[0].text = pt[i]
    cell = table3.cell(i, 1)
    cell.text = pi[i]



doc.add_page_break()



prt4 = doc.add_paragraph('\n Подключение электропривода ', styleTitle)

pt = ['Диаметр и тип кабеля / сечение жилы', 'Сальниковые вводы  стандарт  под небронированный  кабель до 20мм (стандарт)', 'Кабельные вводы количество', 'Сальниковые вводы  стандарт  под небронированный  кабель до 20мм (стандарт)', 'Вводы под бронированный кабель', 'Вводы под бронированный кабель в металлорукаве', 'Вводы под металлорукав', 'Особые требования ']
pi = [' мм ', ' мм ',  ' шт ', ' 3шт ', ' шт и диаметр кабеля по броне ', ' шт и диаметр кабеля по броне ', ' шт и диаметр кабеля по броне ', '\n \n \n']

table4 = doc.add_table(rows=0, cols=2)
table4.style = 'Table Grid'
for i in range(len(pt)):
    row_cells = table4.add_row().cells
    row_cells[0].text = pt[i]
    cell = table4.cell(i, 1)
    cell.text = pi[i]


doc.save('Опросный лист.docx')
convert('Опросный лист.docx', 'Опросный лист.pdf')